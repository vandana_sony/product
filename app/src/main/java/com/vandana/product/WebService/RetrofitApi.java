package com.vandana.product.WebService;
import com.vandana.product.Model.AddProductResponse;
import com.vandana.product.Model.ProductListModel;
import com.vandana.product.Model.ProductResponse;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitApi {

    @GET("view_product.php")
    Call<ProductResponse> productList();


    @Multipart
    @POST("add_product.php")
    Call<AddProductResponse> addProduct(@Part("image1\"; filename=\"myfile.jpg\" ") RequestBody file,
                                    @Part("name") RequestBody name,
                                    @Part("description") RequestBody description,
                                    @Part("regular_price") RequestBody regular_price,
                                    @Part("sale_price") RequestBody sale_price,
                                    @Part("stores") RequestBody stores,
                                    @Part("color") RequestBody color);

    @Multipart
    @POST("update_product.php")
    Call<AddProductResponse> updateProduct(@Part("image1\"; filename=\"myfile.jpg\" ") RequestBody file,
                                        @Part("name") RequestBody name,
                                        @Part("description") RequestBody description,
                                        @Part("regular_price") RequestBody regular_price,
                                        @Part("sale_price") RequestBody sale_price,
                                        @Part("stores") RequestBody stores,
                                        @Part("color") RequestBody color,
                                        @Part("id") RequestBody id);
    @FormUrlEncoded
    @POST("delete_product.php")
    Call<ProductListModel> DeleteProduct(
            @Field("id") String id);


}