package com.vandana.product.WebService;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vandana.product.Model.ProductListModel;
import com.vandana.product.Model.ProductResponse;

import java.util.ArrayList;
import java.util.List;


public class Shprefrences {

    public static final String PREFERENCES = "APPTRACKER";
    Context context;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    public void clearData()
    {
        editor.remove(PREFERENCES);
        editor.clear();
        editor.commit();
    }

    public Shprefrences(Context context) {
        this.context = context;
        sharedpreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public void setString(String key, String val) {
        editor.putString(key, val);
        editor.commit();
    }

    public void setBoolean(String key, boolean val) {
        editor.putBoolean(key, val);
        editor.commit();
    }

    public void setInt(String key, int val) {
        editor.putInt(key, val);
        editor.commit();
    }


    public String getString(String key, String val) {
        return sharedpreferences.getString(key,val).toString();
    }

    public boolean getBoolean(String key, boolean val) {
        return sharedpreferences.getBoolean(key,val);
    }


    public void setProductListModel(String key, ProductResponse obj)
    {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        editor.putString(key, json);
        editor.commit();
    }

    public ProductListModel getList(String key)
    {
        Gson gson = new Gson();
        String json = sharedpreferences.getString(key, "");
        ProductListModel ob= gson.fromJson(json, ProductListModel.class);
        return ob;
    }



}
