package com.vandana.product.Adapter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;


import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vandana.product.Activity.UpdateProductActivity;
import com.vandana.product.Model.ProductListModel;
import com.vandana.product.Model.ProductResponse;
import com.vandana.product.R;
import com.vandana.product.WebService.ApiClient;
import com.vandana.product.WebService.RetrofitApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
        private ArrayList<ProductListModel> productListModelList;

        public DataAdapter(ArrayList<ProductListModel> productListModelList) {
            this.productListModelList = productListModelList;
            notifyDataSetChanged();

        }


        @Override
        public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_product, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {

            viewHolder.txtName.setText(productListModelList.get(i).getName());
            viewHolder.txtDes.setText(productListModelList.get(i).getDescription());
            viewHolder.txtRegular.setText(productListModelList.get(i).getRegular_price());
            viewHolder.txtSale.setText(productListModelList.get(i).getSale_price());
            viewHolder.txtColor.setText(productListModelList.get(i).getColor());
            viewHolder.txtStores.setText(productListModelList.get(i).getStores());

            String uri = productListModelList.get(i).getImage1(); // or whatever you want
            Glide.with(viewHolder.itemView.getContext()).load(uri).into(viewHolder.imgProduct);

            viewHolder.btn_popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popupmenu = new PopupMenu(v.getContext(), v);

                    popupmenu.getMenuInflater().inflate(R.menu.popup_menu, popupmenu.getMenu());
                    popupmenu.getMenu().findItem(R.id.edit).setVisible(true);
                    popupmenu.setOnMenuItemClickListener(item -> {
                        switch (item.getItemId()) {
                        case R.id.delete:
                            deleteGatePass(productListModelList.get(i).getId());
                            notifyItemRemoved(i);
                            break;

                            case R.id.edit:
                                Log.e("id", productListModelList.get(i).getId());

                                Intent intent=new Intent(v.getContext(), UpdateProductActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                v.getContext().startActivity(intent);

                        }
                        return true;

                    });


                    popupmenu.show();
                }

            });


        }

        @Override
        public int getItemCount() {
//            return productListModelList.size();
            if(productListModelList != null){
                return productListModelList.size();
            }
            return 0;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView txtName,txtDes,txtColor,txtRegular,txtSale,txtStores;
            ImageView imgProduct,btn_popup;
            public ViewHolder(View view) {
                super(view);

                txtName = (TextView) itemView.findViewById(R.id.txtName);
                txtDes = (TextView) itemView.findViewById(R.id.txtDes);
                txtColor = (TextView) itemView.findViewById(R.id.txtColor);
                txtRegular = (TextView) itemView.findViewById(R.id.txtRegular);
                txtSale = (TextView) itemView.findViewById(R.id.txtSale);
                txtStores = (TextView) itemView.findViewById(R.id.txtStores);
                imgProduct=(ImageView)itemView.findViewById(R.id.imgProduct);
                btn_popup = itemView.findViewById(R.id.btn_popup);


            }
        }


        public void deleteGatePass(String id) {
        RetrofitApi apiService = ApiClient.getClient().create(RetrofitApi.class);
        Call<ProductListModel> call = apiService.DeleteProduct(id);
        call.enqueue(new Callback<ProductListModel>() {
            @Override
            public void onResponse(Call<ProductListModel> call, Response<ProductListModel> response) {

            }

            @Override
            public void onFailure(Call<ProductListModel> call, Throwable t) {
            }
        });
    }

    }

