package com.vandana.product.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.vandana.product.Model.AddProductResponse;

import com.vandana.product.R;
import com.vandana.product.WebService.ApiClient;
import com.vandana.product.WebService.RetrofitApi;
import com.vandana.product.WebService.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProductActivity extends AppCompatActivity {

    @BindView(R.id.spnColor)
    Spinner spnColor;


    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtDes)
    EditText edtDes;

    @BindView(R.id.edtStores)
    EditText edtStores;

    @BindView(R.id.edtRegular)
    EditText edtRegular;

    @BindView(R.id.edtSale)
    EditText edtSale;

    @BindView(R.id.imgProduct)
    ImageView imageView;

    @BindView(R.id.btnUpload)
    Button btnUpload;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    Uri fileUri;

    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });

        btnSubmit.setOnClickListener(v -> {
            String edtName1 = edtName.getText().toString();
            String edtDes1 = edtDes.getText().toString();
            String edtRegular1 = edtRegular.getText().toString();
            String edtSale1 = edtSale.getText().toString();
            String edtStores1 = edtStores.getText().toString();
            String spnColor1 = spnColor.getSelectedItem().toString();

            if (edtName.equals("")) {
                Toast.makeText(UpdateProductActivity.this, "Fill Name", Toast.LENGTH_LONG).show();
                return;
            } else {
                //  progress_bar.setVisibility(View.VISIBLE);
                // Data data_model= FastSave.getInstance().getObject("login_data",Data.class);
                uploadFile(imageImagePath,edtName1,edtDes1,edtRegular1,edtSale1,edtStores1,spnColor1,id);
                onBackPressed();
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateProductActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(UpdateProductActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }
    String imageImagePath = "";

   // String imageImagePath = getRealPathFromURI(fileUri);
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imageView.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        imageView.setImageBitmap(bm);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void uploadFile(String fileUrl, String name,String description,String regular_price,String sale_price,String color,String stores,String id) {

        //creating a file
//        RequestBody file = null;
//        File file = new File(getRealPathFromURI(fileUri));
        RequestBody imgFile = null;
        File imagPh = new File(fileUrl);
        //creating request body for file
        if (imagPh != null && (fileUrl!=null && !fileUrl.equalsIgnoreCase("")))
            imgFile = RequestBody.create(MediaType.parse("image/*"), imagPh);
        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody description1 = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody regular_price1 = RequestBody.create(MediaType.parse("text/plain"), regular_price);
        RequestBody sale_price1 = RequestBody.create(MediaType.parse("text/plain"), sale_price);
        RequestBody color1 = RequestBody.create(MediaType.parse("text/plain"), color);
        RequestBody stores1 = RequestBody.create(MediaType.parse("text/plain"), stores);
        RequestBody id1 = RequestBody.create(MediaType.parse("text/plain"), id);


        RetrofitApi apiService = ApiClient.getClient().create(RetrofitApi.class);
        Call<AddProductResponse> call = apiService.updateProduct(imgFile, name1, description1, regular_price1, sale_price1, color1, stores1,id1);
        call.enqueue(new Callback<AddProductResponse>() {

            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                // if (response.errorBody() == null && response.body() != null) {
//                if (!response.body().error) {
//                    Toast.makeText(getApplicationContext(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Some error occurred...", Toast.LENGTH_LONG).show();
//                }

                Toast.makeText(getApplicationContext(), "Profile Update Successfully...", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

