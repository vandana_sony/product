package com.vandana.product.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.google.gson.Gson;
import com.vandana.product.Adapter.DataAdapter;
import com.vandana.product.Model.ProductListModel;
import com.vandana.product.Model.ProductResponse;
import com.vandana.product.R;
import com.vandana.product.WebService.ApiClient;
import com.vandana.product.WebService.RetrofitApi;
import com.vandana.product.WebService.Shprefrences;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends AppCompatActivity {

    ImageView imgBack;
    private RecyclerView recyclerview;
    private ArrayList<ProductListModel> productListModelList;
    private DataAdapter dataAdapter;

    Shprefrences sh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        imgBack=(ImageView)findViewById(R.id.imgBack);
        imgBack.setOnClickListener(v -> onBackPressed());
        initViews();



    }

    private void initViews(){
        recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(layoutManager);
      // recyclerview.setAdapter(dataAdapter);



        loadJSON();
    }

    private void loadJSON(){
        RetrofitApi apiService = ApiClient.getClient().create(RetrofitApi.class);
        Call<ProductResponse> call = apiService.productList();
        call.enqueue(new Callback<ProductResponse>() {

            @Override
                public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                ProductResponse productResponse =response.body();


                productListModelList = new ArrayList<>(Arrays.asList(productResponse.getResponse()));
                dataAdapter = new DataAdapter(productListModelList);
                recyclerview.setAdapter(dataAdapter);


            }


            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }

        });
    }
}
